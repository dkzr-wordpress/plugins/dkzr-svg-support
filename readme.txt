=== dkzr SVG Support ===
Contributors: joostdekeijzer
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=j@dkzr.nl&item_name=dkzr-svg-support+WordPress+plugin&item_number=Joost+de+Keijzer&currency_code=EUR
Tags: media, svg
Requires at least: 5.0
Tested up to: 5.9
Stable tag: 2.0
Requires PHP: 7.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Enable SVG media upload for users with 'unfiltered_html' capability.

== Description ==

This plugin allows users with the 'unfiltered_html' capability (administrators & editors in single site; super-admins in multisite) to upload SVG images.

== Installation ==

* Download the plugin
* Uncompress it with your preferred unzip program
* Copy the entire directory in your plugin directory of your WordPress blog (/wp-content/plugins)
* Activate the plugin
* Try and upload a SVG image!

== Changelog ==

= v2.1 =

* Changed required capability from 'admininistrator' to 'unfiltered_html'

= v2.0 =

* Added SVG viewbox parsing to try and retreive image width & height

= v1.0 =

* Initial release

== Upgrade Notice ==
