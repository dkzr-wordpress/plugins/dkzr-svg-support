<?php
/**
 * Plugin Name: dkzr SVG Support
 * Plugin URI: https://dkzr.nl
 * Update URI: https://api.dkzr.nl/wp/update-check/
 * Composer Package Name: dkzr/svg-support
 * Description: Enable SVG media upload for users with 'unfiltered_html' capability.
 * Author: Joost de Keijzer
 * Version: 2.2.1
 */
class dkzr_svg_support {
  public function __construct() {
    add_filter( 'upload_mimes', array( $this, 'mimes' ) );
    add_filter( 'wp_generate_attachment_metadata', array( $this, 'metadata' ), 10, 2 );
  }

  function mimes( $mimes ) {
    // @see https://wordpress.org/support/article/roles-and-capabilities/#unfiltered_html
    if( current_user_can( 'unfiltered_html' ) ) {
      // allow SVG file upload
      $mimes['svg']   = 'image/svg+xml';
    }

    return $mimes;
  }

  public function metadata( $metadata, $attachment_id ) {
    if ( 'image/svg+xml' == get_post_mime_type( $attachment_id ) ) {
      if ( empty( $metadata ) ) {
        $metadata = array();
      }
      $metadata += $this->get_metadata( $attachment_id );
    }

    return $metadata;
  }

/**
 * @return array Array containing the image width and height
 */
  private function get_metadata( $id ) {
    $file = get_attached_file( $id );
    if ( ! $file || ! ( $svg = simplexml_load_file( $file ) ) ) {
      return array();
    }
    $attr = $svg->attributes();

    $width  = $attr->width  ? (int) $attr->width  : null;
    $height = $attr->height ? (int) $attr->height : null;

    if ( ! isset( $width, $height ) && $attr->viewBox ) {
      $viewBox = explode( ' ', trim( preg_replace( '/[,\s]+/', ' ', $attr->viewBox ) ) );

      if ( ! isset( $width ) && isset( $viewBox[2] ) ) {
        $width = (int) $viewBox[2];
      }
      if ( ! isset( $height ) && isset( $viewBox[3] ) ) {
        $height = (int) $viewBox[3];
      }
    }

    $output = array(
      'file'    => $file,
      'width'   => $width,
      'height'  => $height,
    );

    return $output;
  }
}
$dkzrSvgSupport = new dkzr_svg_support();
